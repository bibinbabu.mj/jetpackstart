package com.example.mymaterial

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.mymaterial.ui.theme.MyMaterialTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyMaterialTheme {
                Surface(color = MaterialTheme.colors.background) {
                    mCreateCard()
                }

            }
        }
    }
}

@Composable
fun mCreateCard() {
    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        Card(
            modifier = Modifier
                .width(200.dp)
                .height(400.dp)
                .padding(12.dp), elevation = 4.dp,
            shape = RoundedCornerShape(corner = CornerSize(15.dp)),
            backgroundColor = Color.White
        ) {
            createColoums()

        }
    }
}

@Composable
fun createColoums() {
    val buttonClickState = remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier.height(300.dp),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        mProfileImage()
        mText("Bibin Babu", 20)
        Divider(color = Color.LightGray, thickness = 1.dp)
        mCreateInfo()

        Button(onClick = {
            buttonClickState.value = !buttonClickState.value

        }) {
            Text(
                text = "Protfolio",
                style = MaterialTheme.typography.button
            )
        }


        if (buttonClickState.value) {
            content()
        } else {

        }
    }


}

@Composable
fun mText(name: String, textSize: Int) {
    Text(text = name, fontFamily = FontFamily.Monospace, fontSize = textSize.sp)


}

@Composable
fun mCreateInfo() {
    Column(
        verticalArrangement = Arrangement.Top,
    ) {
        mText(name = "Android Developer", textSize = 14)
        mText(name = "bibinbabumj93@gmail.com", textSize = 14)
        mText(name = "+44 7404436622", textSize = 14)

    }
}

@Composable
fun mProfileImage(modifier: Modifier = Modifier) {
    Surface(
        modifier = modifier
            .size(150.dp)
            .padding(5.dp),
        shape = CircleShape,
        border = BorderStroke(0.5.dp, Color.LightGray),
        elevation = 4.dp,
        color = MaterialTheme.colors.onSurface.copy(alpha = 0.1f)
    ) {

        Image(
            painter = painterResource(id = R.drawable.bibin),
            contentDescription = "Profile image",
            modifier = modifier.size(160.dp),
            contentScale = ContentScale.FillBounds
        )
    }

}

@Preview
@Composable
fun content() {
    Box(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .padding(5.dp)
    ) {
        Surface(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .padding(3.dp),
            shape = RoundedCornerShape(corner = CornerSize(5.dp)),
            border = BorderStroke(2.dp, color = Color.LightGray)
        ) {

            mCreatePortFolio(data = listOf("Project1", "Project2", "Project3", "Project4"))
        }
    }

}

@Composable
fun mCreatePortFolio(data: List<String>) {
    LazyColumn {
        items(data) { item ->
            Card(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(5.dp),
                shape = RectangleShape
            ) {
                Row(modifier = Modifier.padding(8.dp)) {
                    mProfileImage(modifier = Modifier.size(100.dp))
                    Column(
                        modifier = Modifier
                            .align(alignment = Alignment.CenterVertically)
                    ) {
                        Text(text = item, fontWeight = FontWeight.Bold)
                        Text(text = "second", style = MaterialTheme.typography.body2)
                    }
                }
            }
        }
    }

}

//@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MyMaterialTheme {
        mCreateCard()
    }
}